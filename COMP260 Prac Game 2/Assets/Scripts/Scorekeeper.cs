﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scorekeeper : MonoBehaviour {
    public int scorePerGoal = 1;
    private int[] score = new int[2];
    public int[] win = new int[2];
    public Text[] scoreText;
    public Text[] winText;
    public bool showText = false;
    public GameObject panel;

    // Use this for initialization
    void Start () {
        // subscribe to events from all the Goals
        Goal[] goals = FindObjectsOfType<Goal>();

        for (int i = 0; i < goals.Length; i++)
        {
            goals[i].scoreGoalEvent += OnScoreGoal;
        }
        ResetScore();

       
    }

    // Update is called once per frame
    void Update () {
	
	}
    public void OnScoreGoal(int player)
    {
        // add points to the player whose goal it is
        score[player] += scorePerGoal;
        scoreText[player].text = score[player].ToString();
        if (score[player] >= 10)
        {
            showText = true;
            if (showText)
            {

            }
        }
        Debug.Log("Player " + player + ": " + score[player]);
    }

    public void ResetScore()
    {
        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
    }
   
    

}
