﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]

public class Goal : MonoBehaviour {
    public delegate void ScoreGoalHandler(int player);
    public ScoreGoalHandler scoreGoalEvent;
    public int player;  
    public AudioClip scoreClip;
    private AudioSource audio;
    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider collider)
    {
        // play score sound
        audio.PlayOneShot(scoreClip);
        // reset the puck to its starting position
        PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();

        if (scoreGoalEvent != null)
        {
            scoreGoalEvent(player);
        }
    }
}
